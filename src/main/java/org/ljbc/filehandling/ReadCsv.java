package org.ljbc.filehandling;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReadCsv {

	private static final Logger LOGGER = LogManager.getLogger(ReadCsv.class);

	public static void main(String[] args) {

		Scanner fileScanner = null;
		try {
			// creates an scanner
			fileScanner = new Scanner(ReadCsv.class.getResourceAsStream("/zip.txt"));
			// sets the delimiter pattern, in this case either a comma or a new line
			fileScanner.useDelimiter("[,\n]");
			// while is similar to for, but only needs a boolean condition to continue
			while (fileScanner.hasNext()) {
				// gets the first element in the line as int
				int zip = fileScanner.nextInt();
				// gets the second element as String
				String city = fileScanner.next();
				// prints in the log
				LOGGER.info("the zip code {} belongs to {}", zip, city);
			}
		} catch ( Exception e) {
			LOGGER.error("unexpected error while opening the file", e);
		} finally {
			fileScanner.close(); // closes the scanner
		}

	}
}
