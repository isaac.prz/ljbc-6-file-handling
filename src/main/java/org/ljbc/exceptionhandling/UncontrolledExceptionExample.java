package org.ljbc.exceptionhandling;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class just print in console a number that you enter there. But if you
 * enter a letter instead, we will tell the notify the user about the wrong
 * input.
 * 
 * @author Isaac
 *
 */
public class UncontrolledExceptionExample {

	private static final Logger LOGGER = LogManager.getLogger(UncontrolledExceptionExample.class);

	public static void main(String[] args) {
		System.out.println("please enter a number:");
		int number = readNumberFromInput();
		System.out.println("your number is " +  number);

	}

	public static int readNumberFromInput() {
		Scanner myConsoleScanner = new Scanner(System.in);
		return myConsoleScanner.nextInt();
	}

}
