package org.ljbc.exceptionhandling;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class just print in console a number that you enter there.
 * But if you enter a letter instead of a number you will see an unexpected outcome
 * @author Isaac
 *
 */
public class ControlledExceptionExample{

	public static void main(String[] args) {
		System.out.println("please enter a number:");
		try {
			int number = readNumberFromInput();

			System.out.println("your number is " +  number);
		} catch (InputMismatchException exception) {

			System.out.println("your havent enter a number :(  ");
			// normally we should use here a logger
			// but in case you dont have any at least call the printStackTrace to show the error
			exception.printStackTrace();
		}
	}
	
	public static int readNumberFromInput()
	{
		Scanner myConsoleScanner = new Scanner(System.in);
		return myConsoleScanner.nextInt();
	}

}
