# ljbc-6-file-handling

Learn java by challenge lesson 6: learn how to read and write  information from files 


## Getting started

### Checkout this Git repository
 * clone the repository `git clone git@gitlab.com:isaac.prz/ljbc-6-file-handling.git`
 * import it as **existing maven project** 
 
 
## Part 1: Exception handling

### Theory
Before starting with the file manipulation we need to now about exceptions.
Let's say thay you are trying to do something, for instance reading a big file from an usb drive and at once you remove the stick. For sure something will crash, but the way java has to tell you about unexpected events is *throwing an exception*

Lets check the example _UncontrolledExceptionExample_ : This class just print in console a number that you enter there.
 ![Run UncontrolledExceptionExample](docs/run-without-exception.png)
But if you enter a letter instead of a number you will see an unexpected outcome
 ![Run UncontrolledExceptionExample](docs/run-with-exception.png)
 
 This is the [InputMismatchException](https://docs.oracle.com/javase/8/docs/api/java/util/InputMismatchException.html) that is thrown by the the method [Scanner.nextInt](https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html#nextInt--)(check the documentation of the method to see what other kinds of exceptions can be thrown)
 ![Exceptions-Scanner.nextInt](docs/Exceptions-nextInt.png) 
 
 
If you try now to run the _ControlledExceptionExample_ you will find something like this:
 ![Run ControlledExceptionExample](docs/run-controlled-exception.png)
in this case we entered a wrong input again, using try/catch we can control the outcome in case of that exception appears and notify the user what he did wrong.
 
 
 Take a look into these tutorials to check other posibilities that the exceptions offer:
  * [java try-catch-finally at w3schools](https://www.w3schools.com/java/java_try_catch.asp) 
  * [deeper tutorial from oracle as reference](https://docs.oracle.com/javase/tutorial/essential/exceptions/index.html)
 
## Challenges
1. create a copy of the _ControlledExceptionExample_ to catch additionally an exception [NoSuchElementException](https://docs.oracle.com/javase/8/docs/api/java/util/NoSuchElementException.html) (add another catch and print a message telling that you forgot to enter any character in the console, although i'm not sure how to produce this error)
1. create a copy of the _ControlledExceptionExample_ to catch additionally an exception [IllegalStateException](https://docs.oracle.com/javase/8/docs/api/java/lang/IllegalStateException.html). To produce the exception just call `myConsoleScanner.close()` directly before reading the `myConsoleScanner.nextInt()`
1. modify the _ControlledExceptionExample_ to print in the console a line, doesnt matter if there is an exception or not.

 
## Part 2: File handling
 
### Theory

a CSV file is an achronym to *comma separated values*, they are a very common way to read and store tables of information, that later you can open with excel.
These files are just text files with values, separated by commas. You will learn here how to manipulate such text files in java.

* how to create, read and delete [java_files at w3schools](https://www.w3schools.com/java/java_files.asp)
* how to work with [java IO streams and buffers](https://docs.oracle.com/javase/tutorial/essential/io/file.html) 
* how to write files with [outputstream at baeldung](https://www.baeldung.com/java-outputstream)


In our example we are reading the file zip.txt and we logged the information
 * the zip.txt is read from our classpath (everything you put on src/main/resources will be copied in together with the compiled files at target/classes, and this is part of the classpath)
 * we use a Scanner simmilar to the console scanner from second lesson to read the values
 * we configure the Scanner to use a values delimiter based on a java pattern that accepts either a new line or a comma, this is something you will learn in future lessons 
 ![Run read-csv](docs/read-csv.png)


## Challenges


1. print in the console all file names from your home folder
1. implement the ReadCsv example with the [OpenCsv library](http://opencsv.sourceforge.net/)
1. create a class that 
 - has a constructor with a fileName and a integer numberCount
 - The class will have a method generate as many fibonacci numbers as configured in numberCount, the will be stored in a List<Long>
 - There will be a method named save() that will persist the fibonacci List in a File, every number in a new line
 - Create a unit test for this class 


Remember there are several ways to do it. Research in internet first to get ideas. 

## Overall Information about  _Learn java by challenge_
 This is a set of projects or challeges, ordered in order to serve as a lesson. Every lesson will progress you into your goal, learning java with focus on enterprise practises. 

## Contributing
 Feel free to contribute any commentary, constructive critique, or PR with proposed changes is wellcome

## Authors and acknowledgment
 * Isaac Perez (main author)

## License

Copyright 2021 Isaac Perez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information [MIT License](https://opensource.org/licenses/MIT)

